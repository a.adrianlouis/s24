// console.log("Hello World");

//cube
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);


//address
let address = {
	houseNumber: 258,
	street: "Washington Ave NW",
	state: "California",
	zipCode: 90011,
}

let {houseNumber, street, state, zipCode } = address

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

//animal
let animal = {
	name: "Lolong",
	specie: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in",
}

let {name, specie, weight, measurement} = animal

console.log(`${name} was a ${specie}. He weighed at ${weight} with a measurement of ${measurement}.`);


//numbers
let numbers = [1,2,3,4,5];

numbers.forEach((number) =>{
	console.log(number);
});


//reduce
const reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber);


//dog
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let miniDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(miniDog);